package youtube2;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Canal {

    private String nombre;
    private List<Video> videos;
    private Video videoActual;
    private Scanner scanner;
    private Date fechaCreacion;

    public Canal(String nombre) {
        setNombre(nombre);
        videos = new ArrayList<>();
        this.videoActual = null;
        setFechaCreacion(new Date());
        this.scanner = new Scanner(System.in);
    }

    private void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion= fechaCreacion;
	}

	private void setNombre(String nombre) {
		this.nombre= nombre;
		
	}
    private Video getVideoActual() {
    	
    	return videoActual;
    	
    }

	public void menuVideo() {
        int opcion;
        do {
            System.out.println("\nMenu Video - " + videoActual.getTitulo());
            System.out.println("0) Salir al menú del Canal");
            System.out.println("1) Crear nuevo comentario");
            System.out.println("2) Dar like al video");
            System.out.println("3) Mostrar comentarios");
            System.out.println("4) Estadísticas del video");

            System.out.print("Ingrese su opción: ");
            opcion = scanner.nextInt();

            switch (opcion) {
                case 1:
                    crearNuevoComentario();
                    break;
                case 2:
                    videoActual.darLike();
                    
                    break;
                case 3:
                    
                  getmostrarComentarios();
                    return;
                case 4:
                    getVideoActual().estadisticasVideo();
                    break;
                case 0:
                    System.out.println("Volviendo al menú del Canal...");
                    break;
                default:
                    System.out.println("Opción no válida. Inténtelo de nuevo.");
            }
        } while (opcion != 0);
    }

    private void getmostrarComentarios() {
		// TODO Auto-generated method stub
		
	}

	public void crearNuevoVideo() {
        System.out.print("Ingrese el título del nuevo video: ");
        String tituloVideo = scanner.nextLine();
        Video nuevoVideo = new Video(tituloVideo);
        videos.add(nuevoVideo);
        videoActual = nuevoVideo;
        System.out.println("Video creado y seleccionado: " + nuevoVideo);
        menuVideo();
    }

    public void seleccionarVideo() {
        if (videos.isEmpty()) {
            System.out.println("No hay videos disponibles. Creando uno automáticamente...");
            crearNuevoVideo();
        } else {
            System.out.println("\nVideos disponibles:");
            for (int i = 0; i < videos.size(); i++) {
                System.out.println(i + ") " + videos.get(i));
            }

            System.out.print("Seleccione un video (ingrese el número): ");
            int seleccion = scanner.nextInt();

            if (seleccion >= 0 && seleccion < videos.size()) {
                videoActual = videos.get(seleccion);
                System.out.println("Video seleccionado: " + videoActual);
                menuVideo();
            } else {
                System.out.println("Selección no válida.");
            }
        }
    }

    public void estadisticasCanal() {
        // Implementar la visualización de estadísticas del canal
        System.out.println("Estadísticas del canal " + nombre);
    }

    public void crearNuevoComentario() {
        Comentario nuevoComentario = Comentario.crearNuevoComentario();
        videoActual.agregarComentario(nuevoComentario);
        System.out.println("Comentario creado y añadido al video.");

    }


    public void mostrarComentarios() {
        // Implementar la visualización de comentarios del video actual
        System.out.println("Mostrando comentarios del video " + videoActual.getTitulo());
    }

    public void estadisticasVideo() {
        // Implementar la visualización de estadísticas del video actual
        System.out.println("Estadísticas del video " + videoActual.getTitulo());
    }

    
    public String toString() {
        return "Canal{" + "nombre='" + getNombre() + '\'' + ", videos=" + videos + ", videoActual=" + videoActual + '}';
    }

    public String getTituloVideoActual() {
        return videoActual.getTitulo();
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public String getNombre() {
        return nombre;
    }
}
